<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="PartyCast" version="1.4" date="08/01/2020" >
        
		<Author name="Sullemunk" email="" />
		<Description text="Shows Castbar for partymemmbers $lt;br$gt; use '/pc config' for options"/>
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="EA_ChatWindow"/>
			<Dependency name="EA_ActionBars"/>
	    	<Dependency name="LibSlash" />			
		</Dependencies>
        
		<Files>
		      <File name="libs\LibStub.lua" />
				<File name="libs\LibGUI.lua" />
				<File name="libs\LibConfig.lua" />		
		
			<File name="PartyCast.lua"/>
			<File name="PartyCast.xml"/>	
			<File name="PartyCast_config.lua"/>			
			

	
		</Files>
        
		<OnInitialize>
			<CallFunction name="PartyCast.Init"/>
		</OnInitialize>

		<OnUpdate>
				<CallFunction name="PartyCast.Update" /> 
				
				</OnUpdate>	
		<OnShutdown>
		<CallFunction name="PartyCast.OnShutdown" /> 
		</OnShutdown>
		<SavedVariables>
		<SavedVariable name="PartyCast.Settings" global="false"/>		
		</SavedVariables>
	</UiMod>
</ModuleFile>
