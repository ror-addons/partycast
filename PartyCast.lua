if not PartyCast then PartyCast = {} end

local Version = 1.4

local PlayerName = wstring.sub( GameData.Player.name,1,-3 )
local PlayerCareer = GameData.Player.career.line
local GetAbilityData = GetAbilityData
local GetAbilityName = GetAbilityName

local string_format = string.format

local CurrentAbility = nil
local TimerDelay = -1
local PlayerIndex = 0
local TimerSet = 0.002
local CastFade = 1.8

local itemIcons = {}
local itemNames = {}

local IsInteract = false

local SmallFrame = {FrameSize = "PartyCastWindow_Template_Small",ImageType = 1,HideInstant = false}
local MediumFrame = {FrameSize = "PartyCastWindow_Template",ImageType = 2,HideInstant = false}
local LargeFrame = {FrameSize = "PartyCastWindow_Template_Large",ImageType = 1,HideInstant = false}
local PlainFrame = {FrameSize = "PartyCastWindow_Template_Plain",ImageType = 1,HideInstant = false}
local SpecialFrame = {FrameSize = "PartyCastWindow_Template_Special",ImageType = 2,HideInstant = false}

local Frame_Anchor

local F_Identifier = L"\25\pa7l\01\e3 "
local P_Identifier = L"\46\97le\114\116 "
local G_Identifier = L"\a6\t9e\22 "

local function CacheItemIcons(abilityId)
	local itemData = DataUtils.GetItems()	
	for index, item in ipairs(itemData) do
		if (item.bonus ~= nil and item.bonus[1] ~= nil and type(item.bonus[1].reference) == "number") then
			local id = item.bonus[1].reference
			itemIcons[id] = item.iconNum or 0
			itemNames[id] = item.name or L""
			if (id == abilityId) then
				return true
			end
		end
	end
	itemIcons[abilityId] = 0
	return false
end

function PartyCast.Init()
	PartyCast.Enabled = false

	RegisterEventHandler(SystemData.Events.PLAYER_START_INTERACT_TIMER, "PartyCast.StartInteract")
	RegisterEventHandler(SystemData.Events.INTERACT_DONE,  "PartyCast.EndCast")
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "PartyCast.StartCast")
	RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "PartyCast.EndCast")
	RegisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "PartyCast.SetbackCast")	
	RegisterEventHandler( SystemData.Events.PLAYER_DEATH, "PartyCast.ON_DEATH" )	
	--RegisterEventHandler( SystemData.Events.GROUP_PLAYER_ADDED,"PartyCast.GROUP_UPDATED" )
	RegisterEventHandler( SystemData.Events.ENTER_WORLD, "PartyCast.GROUP_UPDATED" )
	RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "PartyCast.GROUP_UPDATED" )

  --  RegisterEventHandler( SystemData.Events.GROUP_UPDATED, "PartyCast.GROUP_UPDATED")
  --  RegisterEventHandler( SystemData.Events.BATTLEGROUP_UPDATED, "PartyCast.GROUP_UPDATED")
	
	
	RegisterEventHandler(TextLogGetUpdateEventId("Chat"), "PartyCast.OnChatLogUpdated")		
	
	LibSlash.RegisterSlashCmd("pc", function(input) PartyCast.Command(input) end)
	LibSlash.RegisterSlashCmd("partycast", function(input) PartyCast.Command(input) end)
	
	PartyCast.CastTimers = {}	
	PartyCast.CastInfo = {FromName = L"",State = L"Start",AbilityID = 0,Cast = false,CastTime=0,Icon=0,Name=L"",Morale=0,Type=L"Ability",Target_Name = L"",Target_Type = 0,Target_Icon = 0,IsBugged = false}
	PartyCast.CastTimersMax = {}	
	PartyCast.BeginCast = false
	PartyCast.StopCast = false
	PartyCast.SetBack = false
	PartyCast.groupData = {}


	PartyCast.AttachedID = {}
	
	
	for i=0,5 do	
		CreateWindowFromTemplate("PartyCastWindow_Dynamic"..i, "PartyCast_DynamicWindow_Template", "Root")			
		CreateWindowFromTemplate("PartyCastStaticWindow"..i, "PartyCast_StaticWindow_Template", "Root")	
		LayoutEditor.RegisterWindow( "PartyCastStaticWindow"..i, L"PartyCast "..i, L"PartyCast"..i, false, false, false, nil )
	end	

	PartyCast.CreateFrame()	
	PartyCast.Enabled = true

	if not PartyCast.Settings then PartyCast.Settings = {} end
	if (not PartyCast.Settings.Version) or (tonumber(PartyCast.Settings.Version) < tonumber(Version)) then PartyCast.Default() end
	if not PartyCast.Settings.Selfcast then PartyCast.Settings.Selfcast = false end
	if not PartyCast.Settings.ShowName then PartyCast.Settings.ShowName = true end	
	if not PartyCast.Settings.Static then PartyCast.Settings.Static = false end
	if not PartyCast.Settings.Frame then PartyCast.Settings.Frame = "Large" end	
	if not PartyCast.Settings.Offset then PartyCast.Settings.Offset = 0 end	
	if not PartyCast.Settings.ShowTarget then PartyCast.Settings.ShowTarget = true end
	if not PartyCast.Settings.Colors then PartyCast.Settings.Colors = {
							Channeling=	{DefaultColor.BLUE.r,DefaultColor.BLUE.g,DefaultColor.BLUE.b},
							Success =  	{DefaultColor.GREEN.r,DefaultColor.GREEN.g,DefaultColor.GREEN.b},
							Failed =	{DefaultColor.RED.r,DefaultColor.RED.g,DefaultColor.RED.b},
							Casting =	{DefaultColor.YELLOW.r,DefaultColor.YELLOW.g,DefaultColor.YELLOW.b},
							Instant =	{229,77,253},
							Ability =	{255,255,255},
							Item = 		{DefaultColor.ORANGE.r,DefaultColor.ORANGE.g,DefaultColor.ORANGE.b},
							Interact =	{DefaultColor.TEAL.r,DefaultColor.TEAL.g,DefaultColor.TEAL.b},
							Target_Type = {{255,255,255},{25,255,25},{255,25,25},{75,200,125},{200,75,75},{200,75,200},{75,75,200}}
							} end


	TextLogAddEntry("Chat", 0, L"<icon00057> PartyCast "..towstring(Version)..L" Loaded.")
end

function PartyCast.CreateFrame()
	if PartyCast.Settings.Frame == "Small" then
		PartyCast.Frame = SmallFrame
		Frame_Anchor = "top"
	elseif PartyCast.Settings.Frame == "Medium" then
		PartyCast.Frame = MediumFrame
		Frame_Anchor = "topleft"
	elseif PartyCast.Settings.Frame == "Special" then
		PartyCast.Frame = SpecialFrame
		Frame_Anchor = "top"
	elseif PartyCast.Settings.Frame == "Large" then
		Frame_Anchor = "topleft"
		PartyCast.Frame = LargeFrame
	elseif PartyCast.Settings.Frame == "Plain" then
		Frame_Anchor = "top"
		PartyCast.Frame = PlainFrame
	end	
	
	for i=0 , 5 do
		if DoesWindowExist("PartyCastWindow"..i) then DestroyWindow("PartyCastWindow"..i) end
		CreateWindowFromTemplate("PartyCastWindow"..i, PartyCast.Frame.FrameSize, "Root")
		WindowClearAnchors("PartyCastWindow"..i)
		WindowAddAnchor("PartyCastWindow"..i, Frame_Anchor,"PartyCastStaticWindow"..i, Frame_Anchor, 0,0)	
		WindowSetScale("PartyCastWindow"..i,WindowGetScale("PartyCastStaticWindow"..i))		
		
		WindowSetShowing("PartyCastWindow"..i,true)
		WindowSetAlpha("PartyCastWindow"..i,0)
		WindowSetFontAlpha("PartyCastWindow"..i,0)		
		PartyCast.AttachedID[i] = 0	
		
	end
	return
end


function PartyCast.OnShutdown()	
	UnregisterEventHandler(SystemData.Events.PLAYER_START_INTERACT_TIMER, "PartyCast.StartInteract")
	UnregisterEventHandler(SystemData.Events.INTERACT_DONE,  "PartyCast.EndCast")
	UnregisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "PartyCast.StartCast")
	UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "PartyCast.EndCast")
	UnregisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "PartyCast.SetbackCast")	
	UnregisterEventHandler( SystemData.Events.PLAYER_DEATH, "PartyCast.ON_DEATH" )
	--UnregisterEventHandler( SystemData.Events.GROUP_PLAYER_ADDED,"PartyCast.GROUP_UPDATED" )	
	UnregisterEventHandler(TextLogGetUpdateEventId("Chat"), "PartyCast.OnChatLogUpdated")
	UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "PartyCast.GROUP_UPDATED" )
	UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "PartyCast.GROUP_UPDATED" )	
end

function PartyCast.OnChatLogUpdated(updateType, filterType)
	if( updateType == SystemData.TextLogUpdate.ADDED ) then 			
		if filterType == SystemData.ChatLogFilters.CHANNEL_9 then	
			local _, filterId, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 ) 
				if text:find(L"PartyCast") then
				PartyCast.FetchedText(text)
				end
		end
	end
end

function PartyCast.StartCast(abilityId, isChannel, desiredCastTime, holdCastBar)
	local abilityData = GetAbilityData(abilityId)
	local Is_Channel = nil
	if isChannel == true then
		Is_Channel = 1
	else
		Is_Channel = 0
	end
	if (abilityData.id == 0) then	
		if (itemIcons[abilityId] == nil) then
			CacheItemIcons(abilityId)
		end
		
		PartyCast.CastInfo.FromName = towstring(PlayerName)
		PartyCast.CastInfo.State = L"Start"
		PartyCast.CastInfo.AbilityID = abilityId	
		PartyCast.CastInfo.Cast = Is_Channel
		PartyCast.CastInfo.CastTime = desiredCastTime	
		PartyCast.CastInfo.Icon = itemIcons[abilityId]
		PartyCast.CastInfo.Name = itemNames[abilityId]
		PartyCast.CastInfo.Morale = L"0"
		PartyCast.CastInfo.Type=L"Item"
		PartyCast.CastInfo.Target_Name = L""
		PartyCast.CastInfo.Target_Type = 0		
		PartyCast.CastInfo.Target_Icon = 0
		PartyCast.CastInfo.IsBugged = 0
	else
		PartyCast.CastInfo.FromName = towstring(PlayerName)
		PartyCast.CastInfo.State = L"Start"
		PartyCast.CastInfo.AbilityID = abilityId	
		PartyCast.CastInfo.Cast = Is_Channel
		PartyCast.CastInfo.CastTime = desiredCastTime	
		PartyCast.CastInfo.Icon = abilityData.iconNum
		PartyCast.CastInfo.Name = abilityData.name
		PartyCast.CastInfo.Morale = towstring(abilityData.moraleLevel)
		PartyCast.CastInfo.Type=L"Ability"
		PartyCast.CastInfo.Target_Name,PartyCast.CastInfo.Target_Type,PartyCast.CastInfo.Target_Icon = PartyCast.Target(abilityData)	
		PartyCast.CastInfo.IsBugged = 0
	end	

	

	TimerDelay = TimerSet
	PlayerIndex = 5 	
	PartyCast.BeginCast = true
	
	if PartyCast.Settings.Selfcast == true then 
		WindowSetScale("PartyCastWindow0",WindowGetScale("PartyCastStaticWindow0"))
	end
	
end

function PartyCast.StartInteract()
	local abilityData = GetAbilityData(1911)
	local Is_Channel = 0
	IsInteract = true

		PartyCast.CastInfo.FromName = towstring(PlayerName)
		PartyCast.CastInfo.State = L"Start"
		PartyCast.CastInfo.AbilityID = 0	
		PartyCast.CastInfo.Cast = Is_Channel
		PartyCast.CastInfo.CastTime = GameData.InteractTimer.time	
		PartyCast.CastInfo.Icon = 30446
		PartyCast.CastInfo.Name = GameData.InteractTimer.objectName
		PartyCast.CastInfo.Morale = L"0"
		PartyCast.CastInfo.Type=L"Interact"
		PartyCast.CastInfo.Target_Name = L""
		PartyCast.CastInfo.Target_Type = 0					
		PartyCast.CastInfo.Target_Icon = 0
	TimerDelay = TimerSet
	PlayerIndex = 5 	
	PartyCast.BeginCast = true

end


function PartyCast.EndCast(isCancel, fromQueuedCall)
	if PartyCast.CastTimers[tostring(PlayerName)] == nil then return end

	if PartyCast.ThrottleEnd == true then PartyCast.ThrottleEnd = false; return end
	if PartyCast.CastInfo.CastTime == 0 then return end	--No need to send stop data for instant cast spells

	local Is_Cancel = nil
	if isCancel == true then
		Is_Cancel = 1
	else
		Is_Cancel = 0
	end
	
	if IsInteract == true and PartyCast.CastTimers[tostring(PlayerName)]~= nil and PartyCast.BeginCast == false then
		if PartyCast.CastTimers[tostring(PlayerName)].Current < (PartyCast.CastTimers[tostring(PlayerName)].Max - 0.1) then
		PartyCast.ThrottleEnd = true
		Is_Cancel = 1
		end
	end	
	
	IsInteract = false
	PartyCast.CastInfo.FromName = towstring(PlayerName)
	PartyCast.CastInfo.State = L"Stop"
	PartyCast.CastInfo.Cast = Is_Cancel
	
	TimerDelay = TimerSet
	PlayerIndex = 5 	
	PartyCast.StopCast = true
	
end

function PartyCast.Target(Ability_Data)

local Target_Icon = 52
local Ability_Data = Ability_Data
local Target_Type = Ability_Data.targetType
--0 = "AOE",1 = "Hostile",2 = "Friendly"
	if Target_Type == 0 then 
		if Ability_Data.isDamaging == true then
				return L"Area Effect",2,0
				--d(L"AOE_DAMAGE")
		elseif Ability_Data.isHealing == true and Ability_Data.isBuff == true then 
		return L"Area Effect",1,0
				--d(L"AOE_HEALING")		
		elseif Ability_Data.isHealing == true and Ability_Data.isBuff == false and Ability_Data.isDefensive == true then 
		return L"Area Effect",1,0
				--d(L"AOE_HEALING")						
		elseif Ability_Data.isHealing == false and Ability_Data.isBuff == true and Ability_Data.isDefensive == true then 
			Target_Icon = Icons.GetCareerIconIDFromCareerLine(PlayerCareer )
			return towstring(PlayerName),1,Target_Icon
		end
	end
	
	if Target_Type == 1 then
	
		local tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selfhostiletarget")))
		local tarLevel = TargetInfo:UnitLevel("selfhostiletarget")
		local tarCareerID = TargetInfo:UnitCareer("selfhostiletarget")
		local tarCareerName = TargetInfo:UnitCareerName("selfhostiletarget")
		local tarFriendly = TargetInfo:UnitIsFriendly("selfhostiletarget")
		local tarType = TargetInfo:UnitType("selfhostiletarget")
		local tarTitle = TargetInfo:UnitNPCTitle( "selfhostiletarget")
		
		if tarType == 5 then 	--enemy player
				Target_Icon = Icons.GetCareerIconIDFromCareerLine(tarCareerID )
				return towstring(tarName),2,Target_Icon		
		elseif tarType == 6 then --npc neutral
			Target_Icon = 22652
			return towstring(tarTitle)..L" "..towstring(tarName)..L" ",4,Target_Icon
		elseif tarType == 8 then --npc hostile
			Target_Icon = 22652
			return towstring(tarTitle)..L" "..towstring(tarName)..L" ",2,Target_Icon
		end
		return towstring(tarName),2,Target_Icon
	end
	
	if Target_Type == 2 then
	
		local tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selffriendlytarget")))
		local tarLevel = TargetInfo:UnitLevel("selffriendlytarget")
		local tarCareerID = TargetInfo:UnitCareer("selffriendlytarget")
		local tarCareerName = TargetInfo:UnitCareerName("selffriendlytarget")
		local tarFriendly = TargetInfo:UnitIsFriendly("selffriendlytarget")
		local tarType = TargetInfo:UnitType("selffriendlytarget")
		local Heal_Buff = 0

		Heal_Buff = 1

		if tarType == 0 then
			tarName = towstring(PlayerName)
			Target_Icon = Icons.GetCareerIconIDFromCareerLine(PlayerCareer )
			return towstring(tarName),Heal_Buff,Target_Icon
		elseif tarType == 1 then
			tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selffriendlytarget")))
			Target_Icon = Icons.GetCareerIconIDFromCareerLine(tarCareerID ) 
			return towstring(tarName),Heal_Buff,Target_Icon
		elseif tarType == 3 then
			Target_Icon = Icons.GetCareerIconIDFromCareerLine(tarCareerID ) 
			tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selffriendlytarget")))	
			return towstring(tarName),Heal_Buff,Target_Icon
		elseif tarType == 4 then	
			Target_Icon = 22696
			tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selffriendlytarget")))	
			return towstring(tarName),3,Target_Icon
		elseif tarType == 8 then	
			tarName = towstring(PartyCast.FixString(TargetInfo:UnitName("selffriendlytarget")))	
			return towstring(tarName),0,Target_Icon		
		end
		
		if tarName == nil or tarName == "" then tarName = towstring(PlayerName) end
		
		return towstring(tarName),Heal_Buff,Target_Icon
	end	
	
	return L"",0,0
end

function PartyCast.GROUP_UPDATED()
PlayerName = wstring.sub( GameData.Player.name,1,-3 )
PlayerCareer = GameData.Player.career.line
PartyCast.EndCast(true, false)
PartyCast.CastTimers = {}	
end

function PartyCast.ON_DEATH()
PlayerName = wstring.sub( GameData.Player.name,1,-3 )
PlayerCareer = GameData.Player.career.line
PartyCast.EndCast(true, false)
PartyCast.CastTimers[tostring(PlayerName)] = nil
end


function PartyCast.SendTextStart(ToName,FromName,State,AbilityID,Cast,CastTime,Icon,Name,m_level,Type,Target_Name,Target_Type,Target_Icon)
	SendChatText(P_Identifier..towstring(ToName)..L" PartyCast:"..towstring(FromName)..L":"..towstring(State)..L":"..towstring(AbilityID)..L":"..towstring(Cast)..L":"..towstring(CastTime)..L":"..towstring(Icon)..L":"..towstring(m_level)..L":"..towstring(Type)..L":"..towstring(Name)..L" :"..Target_Name..L":"..towstring(Target_Type)..L":"..towstring(Target_Icon), ChatSettings.Channels[0].serverCmd)	
	return
end
function PartyCast.SendTextStop(ToName,FromName,State,AbilityID,Cast,Name)
	SendChatText(P_Identifier..towstring(ToName)..L" PartyCast:"..towstring(FromName)..L":"..towstring(State)..L":"..towstring(AbilityID)..L":"..towstring(Cast)..L":"..towstring(Name), ChatSettings.Channels[0].serverCmd)	
	return
end
function PartyCast.SendTextSetback(ToName,FromName,Newtime)
	SendChatText(P_Identifier..towstring(ToName)..L" PartyCast:"..towstring(FromName)..L":SetBack:"..towstring(Newtime), ChatSettings.Channels[0].serverCmd)	
	return
end


function PartyCast.FetchedText(text)
local text = text
	if text:find(L"Start") then
		local SPLIT_TEXT = StringSplit(tostring(text), ":")
		local ObjID = 0
		local PlayerNumber = 0
		PartyCast.CastTimers[SPLIT_TEXT[2]] = {}		
		PartyCast.groupData=PartyUtils.GetPartyData()		
				for index,memberData in ipairs(PartyCast.groupData) do 
					if tostring(SPLIT_TEXT[2]) == tostring(memberData.name) then
					PlayerNumber = index
						if PartyCast.Settings.Static == false then	
							if memberData.worldObjNum ~= 0 then
								ObjID = memberData.worldObjNum
								PartyCast.AttachedID[PlayerNumber] = ObjID
								
								MoveWindowToWorldObject("PartyCastWindow_Dynamic"..PlayerNumber, ObjID, 0.9975)--0.9962 for player 0.9975
								ForceUpdateWorldObjectWindow(ObjID,"PartyCastWindow_Dynamic"..PlayerNumber)			
							
								WindowClearAnchors("PartyCastWindow"..PlayerNumber )
								WindowAddAnchor("PartyCastWindow"..PlayerNumber, Frame_Anchor,"PartyCastWindow_Dynamic"..PlayerNumber, Frame_Anchor, 0,PartyCast.Settings.Offset)	
							end	
						elseif PartyCast.Settings.Static == true then						
							WindowClearAnchors("PartyCastWindow"..PlayerNumber )
							WindowAddAnchor("PartyCastWindow"..PlayerNumber, Frame_Anchor,"PartyCastStaticWindow"..PlayerNumber, Frame_Anchor, 0,0)	
						end						
					end
				end
				
		PartyCast.CastTimers[SPLIT_TEXT[2]].Number = PlayerNumber
		PartyCast.CastTimers[SPLIT_TEXT[2]].ObjID = ObjID

			LabelSetText("PartyCastWindow"..PlayerNumber.."Number",L"")
			LabelSetText("PartyCastWindow"..PlayerNumber.."NumberBG",L"")			
		WindowSetAlpha("PartyCastWindow"..PlayerNumber,1)
		WindowSetAlpha("PartyCastWindow"..PlayerNumber.."Timer",1)
		WindowSetFontAlpha("PartyCastWindow"..PlayerNumber,1)		
		WindowSetScale("PartyCastWindow"..PlayerNumber,WindowGetScale("PartyCastStaticWindow"..PlayerNumber))


		local abilityData = GetAbilityData(tonumber(SPLIT_TEXT[4]))
		local texture, x, y, disabledTexture = GetIconData(tonumber(SPLIT_TEXT[7])) 
		local SpellName = towstring((SPLIT_TEXT[10]))
		local Target_Name = towstring((SPLIT_TEXT[11]))
		local Target_Type = tonumber((SPLIT_TEXT[12]))	
		local Target_Icon = 0
		if SPLIT_TEXT[13] ~= nil then
			Target_Icon = tonumber((SPLIT_TEXT[13]))
		end
		local Morale = tonumber(SPLIT_TEXT[8])
		local Morale_Text = L""
		local Morale_TextBG = L""		
		local Type = towstring((SPLIT_TEXT[9]))
		local CastFade_Temp = CastFade
		if Morale > 0 then
			Morale_Text = L"<icon43>"..towstring(Morale)
			Morale_TextBG = L"<icon5>"..towstring(Morale)	
			LabelSetTextColor("PartyCastWindow"..PlayerNumber.."Name",DefaultColor.GOLD.r,DefaultColor.GOLD.g,DefaultColor.GOLD.b)	
			CastFade_Temp = CastFade*1.5
		else
			CastFade_Temp = CastFade
			if Type == L"Ability" then
			LabelSetTextColor("PartyCastWindow"..PlayerNumber.."Name",PartyCast.Settings.Colors.Ability[1],PartyCast.Settings.Colors.Ability[2],PartyCast.Settings.Colors.Ability[3])
			elseif Type == L"Item" then
			LabelSetTextColor("PartyCastWindow"..PlayerNumber.."Name",PartyCast.Settings.Colors.Item[1],PartyCast.Settings.Colors.Item[2],PartyCast.Settings.Colors.Item[3])
			else
			LabelSetTextColor("PartyCastWindow"..PlayerNumber.."Name",PartyCast.Settings.Colors.Interact[1],PartyCast.Settings.Colors.Interact[2],PartyCast.Settings.Colors.Interact[3])
			end
		end
		
		LabelSetText("PartyCastWindow"..PlayerNumber.."Morale",Morale_Text)
		LabelSetText("PartyCastWindow"..PlayerNumber.."MoraleBG",Morale_TextBG)
		
		if SPLIT_TEXT[5] == "1" then	
			PartyCast.CastTimers[SPLIT_TEXT[2]].Is_Channel = true
			PartyCast.CastTimers[SPLIT_TEXT[2]].Current = tonumber(SPLIT_TEXT[6])
			PartyCast.CastTimers[SPLIT_TEXT[2]].Max = tonumber(SPLIT_TEXT[6])
			PartyCast.CastTimers[SPLIT_TEXT[2]].CastEnded = tonumber(SPLIT_TEXT[6]) + CastFade_Temp
--			PartyCast.CastTimers[SPLIT_TEXT[2]].IsBugged = 0
		else		
			PartyCast.CastTimers[SPLIT_TEXT[2]].Is_Channel = false
			PartyCast.CastTimers[SPLIT_TEXT[2]].Current = 0.0
			PartyCast.CastTimers[SPLIT_TEXT[2]].Max = tonumber(SPLIT_TEXT[6])
			PartyCast.CastTimers[SPLIT_TEXT[2]].CastEnded = CastFade_Temp
	--		PartyCast.CastTimers[SPLIT_TEXT[2]].IsBugged = 0
		end		
	
		if PartyCast.Frame.ImageType == 1 then
			DynamicImageSetTexture ("PartyCastWindow"..PlayerNumber.."ButtonIcon", texture, 64, 64)	
		elseif PartyCast.Frame.ImageType == 2 then	
			CircleImageSetTexture ("PartyCastWindow"..PlayerNumber.."ButtonIcon", texture, 32, 32)	
		end
		
		if  tonumber(SPLIT_TEXT[6]) ~= 0 then
				StatusBarSetMaximumValue("PartyCastWindow"..PlayerNumber.."TimerBar", tonumber(SPLIT_TEXT[6])  )
				
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Timer", PartyCast.Settings.Colors.Casting[1], PartyCast.Settings.Colors.Casting[2], PartyCast.Settings.Colors.Casting[3] )	
			StatusBarSetForegroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", PartyCast.Settings.Colors.Casting[1], PartyCast.Settings.Colors.Casting[2], PartyCast.Settings.Colors.Casting[3] )	
			StatusBarSetBackgroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )

				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Button",PartyCast.Settings.Colors.Casting[1], PartyCast.Settings.Colors.Casting[2], PartyCast.Settings.Colors.Casting[3])
				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."ButtonIcon",255,255,255)

				PartyCast.CastTimers[SPLIT_TEXT[2]].CastEnded = nil
						
		else
				StatusBarSetMaximumValue("PartyCastWindow"..PlayerNumber.."TimerBar", -1 )
	
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Timer",PartyCast.Settings.Colors.Instant[1], PartyCast.Settings.Colors.Instant[2], PartyCast.Settings.Colors.Instant[3] )	
			StatusBarSetForegroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", PartyCast.Settings.Colors.Instant[1], PartyCast.Settings.Colors.Instant[2], PartyCast.Settings.Colors.Instant[3] )	
			StatusBarSetBackgroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
				

	
				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Button",PartyCast.Settings.Colors.Instant[1], PartyCast.Settings.Colors.Instant[2], PartyCast.Settings.Colors.Instant[3] )
				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."ButtonIcon",255,255,255)
				
				if PartyCast.Frame.HideInstant == true then
					WindowSetAlpha("PartyCastWindow"..PlayerNumber.."Timer",0)
				end	
				
				PartyCast.CastTimers[SPLIT_TEXT[2]].CastEnded = CastFade_Temp
		end
		
		local Target_Color = PartyCast.Settings.Colors.Target_Type[Target_Type+1]
		local NameOfTarget = towstring(CreateHyperLink(L"TargetName",towstring(Target_Name), {Target_Color[1],Target_Color[2],Target_Color[3]},{}))
		local NameOfTargetBG = towstring(Target_Name)		
		local TargetIcon = L""
		local TargetIconBG = L""
--[[		
		if Target_Icon ~= 0 then 
		local	Icon_texture, _, _, _ = GetIconData(tonumber(Target_Icon))
			TargetIcon = L"<icon"..towstring(Target_Icon)..L">"	
			TargetIconBG = L"<icon000052>"
		end
		--]]
		if Target_Icon ~= 0 then 
		local	Icon_texture, x,y, z = GetIconData(tonumber(Target_Icon))		
			DynamicImageSetTexture("PartyCastWindow"..PlayerNumber.."TargetWindowIcon",Icon_texture,x,y,z)
		if tonumber(Target_Icon) == 22652 or  tonumber(Target_Icon) == 22696 then
			DynamicImageSetTextureDimensions("PartyCastWindow"..PlayerNumber.."TargetWindowIcon",64,64)
		else
			DynamicImageSetTextureDimensions("PartyCastWindow"..PlayerNumber.."TargetWindowIcon",0,0)		
		end
		else
			DynamicImageSetTexture("PartyCastWindow"..PlayerNumber.."TargetWindowIcon","",0,0,0)
		end
		
		
		StatusBarSetBackgroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
		
		if PartyCast.Settings.ShowTarget == true then
			if Target_Name ~= L"" then
				NameOfTarget = NameOfTarget 
				NameOfTargetBG = NameOfTargetBG
			end
		else
			NameOfTarget = L""
			NameOfTargetBG = L""
		end
			if PartyCast.Settings.ShowName == true then
				LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"  "..towstring(SpellName))	
				LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"  "..towstring(SpellName))	
			else
				LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"")	
				LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"")	
			end
		LabelSetText ("PartyCastWindow"..PlayerNumber.."TargetWindowLabel",NameOfTarget)		
		LabelSetText ("PartyCastWindow"..PlayerNumber.."TargetWindowLabelBG",NameOfTargetBG)				
		WindowSetShowing("PartyCastWindow"..PlayerNumber.."TargetWindow",(NameOfTargetBG ~= L"") and PartyCast.Settings.ShowTarget)
		
	elseif text:find(L"SetBack") then		
		local SPLIT_TEXT = StringSplit(tostring(text), ":")
		
		PartyCast.CastTimers[SPLIT_TEXT[2]].Current = PartyCast.CastTimers[SPLIT_TEXT[2]].Max - tonumber(SPLIT_TEXT[4])
					
	elseif text:find(L"Stop") then
		local SPLIT_TEXT = StringSplit(tostring(text), ":")
		local ObjID = 0
		local PlayerNumber = 0

		PartyCast.CastTimers[SPLIT_TEXT[2]] = {}
		PartyCast.CastTimers[SPLIT_TEXT[2]].CastEnded = CastFade
		local groupData=PartyUtils.GetPartyData()
		for index,memberData in ipairs(groupData) do 
			if tostring(SPLIT_TEXT[2]) == tostring(memberData.name) then
			PlayerNumber = index
				if memberData.worldObjNum ~= 0 then
					ObjID = memberData.worldObjNum	
				end				
			end
		end
		local SpellName = towstring((SPLIT_TEXT[6]))
			if PartyCast.Settings.ShowName == true then
				LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"  "..towstring(SpellName))	
				LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"  "..towstring(SpellName))	
			else
			LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"")	
			LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"")
		end
			if WindowGetShowing("PartyCastWindow"..PlayerNumber) == true then
		--		WindowStartAlphaAnimation( "PartyCastWindow"..PlayerNumber, Window.AnimationType.EASE_OUT_HIDE, WindowGetAlpha("PartyCastWindow"..PlayerNumber), 0.0, 1.2, true, 0.0, 0 )	
			else
			--	WindowSetAlpha("PartyCastWindow"..PlayerNumber,0)
			--	WindowSetAlpha("PartyCastWindow"..PlayerNumber.."Number",0)
			--	WindowSetShowing("PartyCastWindow"..PlayerNumber,false)
			end
		
		if SPLIT_TEXT[5] == "1" then
			if WindowGetShowing("PartyCastWindow"..PlayerNumber) == true and PartyCast.Settings.ShowName == true then
				LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"  Interupted!")
				LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"  Interupted!")
				LabelSetTextColor("PartyCastWindow"..PlayerNumber.."Name",PartyCast.Settings.Colors.Failed[1], PartyCast.Settings.Colors.Failed[2], PartyCast.Settings.Colors.Failed[3])
			else
				LabelSetText ("PartyCastWindow"..PlayerNumber.."Name",L"")
				LabelSetText ("PartyCastWindow"..PlayerNumber.."NameBG",L"")				
				LabelSetText("PartyCastWindow"..PlayerNumber.."Number",L"")
				LabelSetText("PartyCastWindow"..PlayerNumber.."NumberBG",L"")				
			end
			
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Timer",PartyCast.Settings.Colors.Failed[1], PartyCast.Settings.Colors.Failed[2], PartyCast.Settings.Colors.Failed[3])
			StatusBarSetForegroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", PartyCast.Settings.Colors.Failed[1], PartyCast.Settings.Colors.Failed[2], PartyCast.Settings.Colors.Failed[3] )
			StatusBarSetBackgroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
			
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Button",PartyCast.Settings.Colors.Failed[1], PartyCast.Settings.Colors.Failed[2], PartyCast.Settings.Colors.Failed[3])
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."ButtonIcon",PartyCast.Settings.Colors.Failed[1], PartyCast.Settings.Colors.Failed[2], PartyCast.Settings.Colors.Failed[3])
			

		else
			if	PartyCast.CastTimers[SPLIT_TEXT[2]].Is_Channel == false then
				PartyCast.CastTimers[SPLIT_TEXT[2]].Max = -1
				PartyCast.CastTimers[SPLIT_TEXT[2]].Current = 1
				--PartyCast.CastTimers[SPLIT_TEXT[2]].IsBugged = 0
				

			end
						
				PartyCast.CastTimers[SPLIT_TEXT[2]].Max = 1
				PartyCast.CastTimers[SPLIT_TEXT[2]].Current = 0
				--PartyCast.CastTimers[SPLIT_TEXT[2]].IsBugged = 0
				StatusBarSetMaximumValue("PartyCastWindow"..PlayerNumber.."TimerBar", 1 )				
				StatusBarSetCurrentValue("PartyCastWindow"..PlayerNumber.."TimerBar", 1 )	
			
			WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Timer",PartyCast.Settings.Colors.Success[1], PartyCast.Settings.Colors.Success[2], PartyCast.Settings.Colors.Success[3] )
			StatusBarSetForegroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", PartyCast.Settings.Colors.Success[1], PartyCast.Settings.Colors.Success[2], PartyCast.Settings.Colors.Success[3] )
			StatusBarSetBackgroundTint( "PartyCastWindow"..PlayerNumber.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
				
				
				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."Button",PartyCast.Settings.Colors.Success[1], PartyCast.Settings.Colors.Success[2], PartyCast.Settings.Colors.Success[3] )
				WindowSetTintColor("PartyCastWindow"..PlayerNumber.."ButtonIcon",255,255,255)
				
		end	
	end	
end


function PartyCast.Update(elapsedTime)


if PartyCast.Enabled == true then

	if PartyCast.CastTimers ~= nil then
	
		for k,v in pairs(PartyCast.CastTimers) do
		
			local ObjID = tonumber(PartyCast.CastTimers[k].ObjID) or 0
			if ObjID > 0 then
			ForceUpdateWorldObjectWindow(ObjID,"PartyCastWindow"..PartyCast.CastTimers[k].Number)	
			end
		

			if PartyCast.CastTimers[k].CastEnded ~= nil then
			if PartyCast.CastTimers[k].CastEnded >= 0 then
			PartyCast.CastTimers[k].CastEnded = PartyCast.CastTimers[k].CastEnded - elapsedTime
			elseif PartyCast.CastTimers[k].CastEnded < 0 then
			PartyCast.CastTimers[k] = nil
			end
			end
				
				
		if PartyCast.CastTimers[k] ~= nil then
				if PartyCast.CastTimers[k].Max == -1 and PartyCast.CastTimers[k].Current ==1 and  PartyCast.CastTimers[k].IsBugged == nil then
				PartyCast.CastTimers[k].IsBugged = 10				
					--d(L"Bugged??") 
				end
				
				if PartyCast.CastTimers[k].Is_Channel == true and PartyCast.CastTimers[k].Current < 0 and PartyCast.CastTimers[k].IsBugged == nil then
				PartyCast.CastTimers[k].IsBugged = 10				
					--d(L"Bugged??") 
				end
				
				
		if PartyCast.CastTimers[k].IsBugged == 10 then
			PartyCast.CastTimers[k].IsBugged = 9
			PartyCast.CastTimers[k].CastEnded = CastFade
		--PartyCast.SendTextStop(memberData.name,PlayerName,L"Stop",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.Name)
		end			
		end
		
		

				
				
				
			
		if PartyCast.CastTimers[k] ~= nil and PartyCast.CastTimers[k].Is_Channel == false then	
			if PartyCast.CastTimers[k].Current < PartyCast.CastTimers[k].Max then
				PartyCast.CastTimers[k].Current = PartyCast.CastTimers[k].Current + elapsedTime	
				StatusBarSetCurrentValue("PartyCastWindow"..PartyCast.CastTimers[k].Number.."TimerBar", PartyCast.CastTimers[k].Current )
				
			local c_TIMER = (PartyCast.CastTimers[k] and (PartyCast.CastTimers[k].Max - PartyCast.CastTimers[k].Current)) or 0.0	
			if	c_TIMER >= 0 then
				c_TIMER = c_TIMER
			else
				c_TIMER = 0.0
			end
				
				
				LabelSetText("PartyCastWindow"..PartyCast.CastTimers[k].Number.."Number", towstring(string_format("%.1f", (c_TIMER))))
				LabelSetText("PartyCastWindow"..PartyCast.CastTimers[k].Number.."NumberBG", towstring(string_format("%.1f", (c_TIMER))))				
			else
				PartyCast.CastTimers[k].Max = -1
				PartyCast.CastTimers[k].Current = 1
			end
		else
		
			if PartyCast.CastTimers[k] ~= nil and PartyCast.CastTimers[k].Current ~= nil then 
				if PartyCast.CastTimers[k].Current > 0 then
				PartyCast.CastTimers[k].Current = PartyCast.CastTimers[k].Current - elapsedTime	
				StatusBarSetCurrentValue("PartyCastWindow"..PartyCast.CastTimers[k].Number.."TimerBar", PartyCast.CastTimers[k].Current )	
				
			WindowSetTintColor("PartyCastWindow"..PartyCast.CastTimers[k].Number.."Timer", PartyCast.Settings.Colors.Channeling[1], PartyCast.Settings.Colors.Channeling[2], PartyCast.Settings.Colors.Channeling[3])
			StatusBarSetForegroundTint( "PartyCastWindow"..PartyCast.CastTimers[k].Number.."TimerBar", PartyCast.Settings.Colors.Channeling[1], PartyCast.Settings.Colors.Channeling[2], PartyCast.Settings.Colors.Channeling[3] )
			StatusBarSetBackgroundTint( "PartyCastWindow"..PartyCast.CastTimers[k].Number.."TimerBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
				


				WindowSetTintColor("PartyCastWindow"..PartyCast.CastTimers[k].Number.."Button", PartyCast.Settings.Colors.Channeling[1], PartyCast.Settings.Colors.Channeling[2], PartyCast.Settings.Colors.Channeling[3] )
				WindowSetTintColor("PartyCastWindow"..PartyCast.CastTimers[k].Number.."ButtonIcon",255,255,255)

				LabelSetText("PartyCastWindow"..PartyCast.CastTimers[k].Number.."Number", towstring(string_format("%.1f", PartyCast.CastTimers[k].Current)))
				LabelSetText("PartyCastWindow"..PartyCast.CastTimers[k].Number.."NumberBG", towstring(string_format("%.1f", PartyCast.CastTimers[k].Current)))								
			else
				--WindowSetShowing("PartyCastWindow"..PartyCast.CastTimers[k].Number.."Number",false)
				--PartyCast.CastTimers[k] = nil	
			end
			end
		end
	
	
		end	
		

		
	end

	if TimerDelay > 0 then
	TimerDelay = TimerDelay - elapsedTime
	else
	if PartyCast.BeginCast == true then	
	--d("Started party send")
		if PartyUtils.IsPartyActive() then
			local groupData=PartyUtils.GetPartyData()	
			if PlayerIndex > 0 then		
			local memberData = groupData[PlayerIndex]		
				if memberData.isDistant == false and memberData.name ~= L"" then
					PartyCast.SendTextStart(memberData.name,PlayerName,L"Start",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.CastTime,PartyCast.CastInfo.Icon,PartyCast.CastInfo.Name,PartyCast.CastInfo.Morale,PartyCast.CastInfo.Type,PartyCast.CastInfo.Target_Name,PartyCast.CastInfo.Target_Type,PartyCast.CastInfo.Target_Icon)
					--d(L"sended Start to: "..memberData.name)
				else
				--	d(L"Skipped "..towstring(PlayerIndex))
				end
				PlayerIndex = PlayerIndex -1
				TimerDelay = TimerSet
			else
			--	if PartyCast.Settings.Selfcast == true then 
					PartyCast.SendTextStart(PlayerName,PlayerName,L"Start",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.CastTime,PartyCast.CastInfo.Icon,PartyCast.CastInfo.Name,PartyCast.CastInfo.Morale,PartyCast.CastInfo.Type,PartyCast.CastInfo.Target_Name,PartyCast.CastInfo.Target_Type,PartyCast.CastInfo.Target_Icon) 
			--	end
			
			PartyCast.BeginCast = false
			end
			else
			--	if PartyCast.Settings.Selfcast == true then 
					PartyCast.SendTextStart(PlayerName,PlayerName,L"Start",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.CastTime,PartyCast.CastInfo.Icon,PartyCast.CastInfo.Name,PartyCast.CastInfo.Morale,PartyCast.CastInfo.Type,PartyCast.CastInfo.Target_Name,PartyCast.CastInfo.Target_Type,PartyCast.CastInfo.Target_Icon) 
			--	end
			PartyCast.BeginCast = false
			end
			--PartyCast.SendTextStart(PlayerName,PlayerName,L"Start",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.CastTime,PartyCast.CastInfo.Icon,PartyCast.CastInfo.Name)
			--PartyCast.BeginCast = false
		end	
		
		if PartyCast.SetBack == true then		
		if PartyUtils.IsPartyActive() then
			local groupData=PartyUtils.GetPartyData()	
			if PlayerIndex > 0 then		
			local memberData = groupData[PlayerIndex]		
				if memberData.isDistant == false and memberData.name ~= L"" then
					PartyCast.SendTextSetback(memberData.name,PlayerName,PartyCast.CastInfo.SetBackTime)			
				--	d(L"sended Setback to: "..memberData.name)
				else
				--	d(L"Skipped "..towstring(PlayerIndex))
				end
				PlayerIndex = PlayerIndex -1
				TimerDelay = TimerSet
			else		
			--	if PartyCast.Settings.Selfcast == true then 
					PartyCast.SendTextSetback(PlayerName,PlayerName,PartyCast.CastInfo.SetBackTime)	
			--	end
			PartyCast.SetBack = false
			end
			else
		--	if PartyCast.Settings.Selfcast == true then 
				PartyCast.SendTextSetback(PlayerName,PlayerName,PartyCast.CastInfo.SetBackTime) 
		--	end
			PartyCast.SetBack = false
			end
			--PartyCast.SendTextSetback(PlayerName,PlayerName,PartyCast.CastInfo.SetBackTime)		
			--PartyCast.SetBack = false		
		end	
	
		
		if PartyCast.StopCast == true and PartyCast.BeginCast == false then	
		--d("Stopped party send")
		if PartyUtils.IsPartyActive() then
			local groupData=PartyUtils.GetPartyData()	
			if PlayerIndex > 0 then		
			local memberData = groupData[PlayerIndex]		
				if memberData.isDistant == false and memberData.name ~= L"" then
					PartyCast.SendTextStop(memberData.name,PlayerName,L"Stop",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.Name)
				--	d(L"sended Start to: "..memberData.name)
				else
				--	d(L"Skipped "..towstring(PlayerIndex))
				end
				PlayerIndex = PlayerIndex -1
				TimerDelay = TimerSet
			else		
			--if PartyCast.Settings.Selfcast == true then 
				PartyCast.SendTextStop(PlayerName,PlayerName,L"Stop",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.Name) 
			--end
			PartyCast.StopCast = false
			PartyCast.SetBack = false	
			end
			else
			--if PartyCast.Settings.Selfcast == true then 
				PartyCast.SendTextStop(PlayerName,PlayerName,L"Stop",PartyCast.CastInfo.AbilityID,PartyCast.CastInfo.Cast,PartyCast.CastInfo.Name) 
			--end
			PartyCast.StopCast = false
			PartyCast.SetBack = false	
			end
		end	
		
	end	
	
 if PartyCast.groupData[1] ~= nil then	
	for i=1,5 do	

 			local P_Data = PartyCast.groupData[i]
			local ObjID = P_Data.worldObjNum
	if PartyCast.Settings.Static == false then
	
			MoveWindowToWorldObject("PartyCastWindow_Dynamic"..i, ObjID, 0.9975)--0.9962 for player 0.9975
			ForceUpdateWorldObjectWindow(ObjID,"PartyCastWindow_Dynamic"..i)					
			WindowClearAnchors("PartyCastWindow"..i )
			WindowAddAnchor("PartyCastWindow"..i, Frame_Anchor,"PartyCastWindow_Dynamic"..i, Frame_Anchor, 0,PartyCast.Settings.Offset)			

			if PartyCast.CastTimers[tostring(P_Data.name)] ~= nil then
				local c_Timer = (PartyCast.CastTimers[tostring(P_Data.name)] and PartyCast.CastTimers[tostring(P_Data.name)].Current) or 0
				local c_Fader = (PartyCast.CastTimers[tostring(P_Data.name)] and PartyCast.CastTimers[tostring(P_Data.name)].CastEnded) or 1
				if WindowGetShowing("PartyCastWindow_Dynamic"..i) == false then
					WindowSetAlpha("PartyCastWindow"..i,0)					
					WindowSetFontAlpha("PartyCastWindow"..i,0)						
				else
					if PartyCast.CastTimers[tostring(P_Data.name)].CastEnded ~= nil then
						WindowSetAlpha("PartyCastWindow"..i,c_Fader)	
						WindowSetFontAlpha("PartyCastWindow"..i,c_Fader)				
					else	
						WindowSetAlpha("PartyCastWindow"..i,c_Fader)
						WindowSetFontAlpha("PartyCastWindow"..i,c_Fader)							
					end
				end
			else
				WindowSetAlpha("PartyCastWindow"..i,0)	
				WindowSetFontAlpha("PartyCastWindow"..i,0)								
			end
	elseif PartyCast.Settings.Static == true then
			if PartyCast.CastTimers[tostring(P_Data.name)] ~= nil then
				local c_Timer = (PartyCast.CastTimers[tostring(P_Data.name)] and PartyCast.CastTimers[tostring(P_Data.name)].Current) or 0
				local c_Fader = (PartyCast.CastTimers[tostring(P_Data.name)] and PartyCast.CastTimers[tostring(P_Data.name)].CastEnded) or 1
	
				WindowSetAlpha("PartyCastWindow"..i,c_Fader)
				WindowSetFontAlpha("PartyCastWindow"..i,c_Fader)						
	
			else			
				WindowSetAlpha("PartyCastWindow"..i,0)	
				WindowSetFontAlpha("PartyCastWindow"..i,0)			
								
			end
		end
		
		end
		
	else
		for i=1,5 do		
			WindowSetAlpha("PartyCastWindow"..i,0)	
			WindowSetFontAlpha("PartyCastWindow"..i,0)	
		end
	end	
	
				if PartyCast.CastTimers[tostring(PlayerName)] ~= nil then
					local c_Timer = (PartyCast.CastTimers[tostring(PlayerName)] and PartyCast.CastTimers[tostring(PlayerName)].Current) or 0				
					local c_Fader = (PartyCast.CastTimers[tostring(PlayerName)] and PartyCast.CastTimers[tostring(PlayerName)].CastEnded) or 1

							if PartyCast.CastTimers[tostring(PlayerName)].CastEnded ~= nil then

								WindowSetAlpha("PartyCastWindow0",c_Fader)	
								WindowSetFontAlpha("PartyCastWindow0",c_Fader)					

							else	
								WindowSetAlpha("PartyCastWindow0",c_Fader)	
								WindowSetFontAlpha("PartyCastWindow0",c_Fader)					
							end
				
					else
						WindowSetAlpha("PartyCastWindow0",0)
						WindowSetFontAlpha("PartyCastWindow0",0)									
					end
	
	WindowSetShowing("PartyCastWindow0",PartyCast.Settings.Selfcast)



---------------------------------------------------------
	if PartyCast.Settings.Frame == "Special" then
		for i=0,5 do
			if WindowGetAlpha("PartyCastWindow"..i) > 0 then
			local CurrBar = StatusBarGetCurrentValue("PartyCastWindow"..i.."TimerBar")
			local MaxBar = 	StatusBarGetMaximumValue("PartyCastWindow"..i.."TimerBar")
			local perc_comp = (CurrBar/MaxBar)
			if perc_comp <= 0 then perc_comp = 1 end
			WindowClearAnchors("PartyCastWindow"..i.."NewCastScroll_scrollStuff")
			WindowAddAnchor( "PartyCastWindow"..i.."NewCastScroll_scrollStuff", "top", "PartyCastWindow"..i.."NewCastScroll_scroll", "top",1,-(55-(50*perc_comp)))
			WindowClearAnchors( "PartyCastWindow"..i.."NewCast")
			WindowAddAnchor( "PartyCastWindow"..i.."NewCast" , "bottom", "PartyCastWindow"..i, "bottom", 0,64-(50*perc_comp))	
			WindowSetTintColor("PartyCastWindow"..i.."NewCastScroll_scrollStuff",WindowGetTintColor("PartyCastWindow"..i.."TimerBar")) 
			end
		end
	end
---------------------------------------------------------------

end	


end


function PartyCast.SetbackCast(newtime)
	PartyCast.CastInfo.FromName = towstring(PlayerName)
	PartyCast.CastInfo.SetBackTime = tonumber(newtime)
	TimerDelay = TimerSet
	PlayerIndex = 5 	
	PartyCast.SetBack = true
end


function PartyCast.Command(input)
	local input1 = nil
	local input2 = nil
	
	input1 = string.sub(input,0,string.find(input," "))
	if string.find(input," ") ~= nil then
		input1 = string.sub(input,0,string.find(input," ")-1)
		input2 = string.sub(input,string.find(input," ")+1,-1)
	end


	if (input1 == "large") then
		PartyCast.Settings.Frame = "Large"
		PartyCast.CreateFrame()
	elseif (input1 == "medium") then
		PartyCast.Settings.Frame = "Medium"
		PartyCast.CreateFrame()
	elseif (input1 == "small") then
		PartyCast.Settings.Frame = "Small"
		PartyCast.CreateFrame()
	elseif (input1 == "special") then
		PartyCast.Settings.Frame = "Special"
		PartyCast.CreateFrame()	
	elseif (input1 == "plain") then
		PartyCast.Settings.Frame = "Plain"
		PartyCast.CreateFrame()			
	elseif (input1 == "selfcast") then
		PartyCast.Settings.Selfcast = not PartyCast.Settings.Selfcast
	elseif (input1 == "static") then
		PartyCast.Settings.Static = not PartyCast.Settings.Static
	elseif (input1 == "showtarget") then
		PartyCast.Settings.ShowTarget = not PartyCast.Settings.ShowTarget	
	elseif (input1 == "showname") then	
		PartyCast.Settings.ShowName = not PartyCast.Settings.ShowName	 		
	elseif (input1 == "reset") then
		PartyCast.Default()	
	elseif (input1 == "offset") then
		PartyCast.Settings.Offset = tonumber(input2)
	elseif (input1 == "config") then
		PartyCast_config.Slash(input2)		
	else 
	return
	end
end

function PartyCast.FixString(str)
	if (str == nil) then return nil end
	local str = str
	local pos = str:find (L"^", 1, true)
	if (pos) then str = str:sub (1, pos - 1) end	
	return str
end

function PartyCast.Default()
PartyCast.Settings = {}
PartyCast.Settings.Selfcast = false 
PartyCast.Settings.ShowTarget = true 
PartyCast.Settings.Static = false 
PartyCast.Settings.ShowName = true 
PartyCast.Settings.Frame = "Large" 	
PartyCast.Settings.Offset = 0 
PartyCast.Settings.Version = Version
PartyCast.Settings.Colors = {
							Channeling=	{DefaultColor.BLUE.r,DefaultColor.BLUE.g,DefaultColor.BLUE.b},
							Success =  	{DefaultColor.GREEN.r,DefaultColor.GREEN.g,DefaultColor.GREEN.b},
							Failed =	{DefaultColor.RED.r,DefaultColor.RED.g,DefaultColor.RED.b},
							Casting =	{DefaultColor.YELLOW.r,DefaultColor.YELLOW.g,DefaultColor.YELLOW.b},
							Instant =	{229,77,253},
							Ability =	{255,255,255},
							Item = 		{DefaultColor.ORANGE.r,DefaultColor.ORANGE.g,DefaultColor.ORANGE.b},
							Interact =	{DefaultColor.TEAL.r,DefaultColor.TEAL.g,DefaultColor.TEAL.b},
							Target_Type = {{255,255,255},{25,255,25},{255,25,25},{75,200,125},{200,75,75},{200,75,200},{75,75,200}}
							} 
local T_Scale = InterfaceCore.GetScale()
WindowSetScale("PartyCastStaticWindow0",T_Scale)
for i=1,5 do							
WindowSetScale("PartyCastStaticWindow"..i,T_Scale*0.8)
end
PartyCast.CreateFrame()
TextLogAddEntry("Chat", 0, L"PartyCast Settings has been resetted to Default")
end