-- loads an instance of LibConfig
LibConfig = LibStub("LibConfig")

PartyCast_config = {}

local GUI

function PartyCast_config.Slash(msg)
  if (not GUI) then
    -- parameters: title text, settings table, callback-function
    -- note that you need to have a settings table!
    GUI = LibConfig("PartyCast Configuration", PartyCast.Settings, true, PartyCast_config.SettingsChanged)
    
	GUI:AddTab("General")
    	-- page 1
    	local label = GUI("label", "Your General Settings For PartyCaster.").label
		label:Align("left")
		
		local comboBox = GUI("combobox", "Frame:","Frame").combo
		comboBox:Add("Large")
    	comboBox:Add("Medium")
    	comboBox:Add("Small")
    	comboBox:Add("Plain")
		comboBox:Add("Special")
		
		GUI("checkbox", "Show Selfcast", "Selfcast")
		GUI("checkbox", "Show Target icon/Name", "ShowTarget")		
		GUI("checkbox", "Show Ability Name", "ShowName")			
		GUI("checkbox", "Use Static Placements", "Static")	
		GUI("textbox", "Frame Offset (not self and static placements)", "Offset")		
    
    GUI:AddTab("Color Options")

    	label = GUI("label", "Here You Can Set The Colors of The Different Actions On The Castbars And Fonts.").label
    	label:Align("left")
		
		GUI("color", "Instant cast", {"Colors","Instant"})
		GUI("color", "Successfull casting", {"Colors","Success"})
		GUI("color", "Failed/Interrupted", {"Colors","Failed"})
  		GUI("color", "Casting Ability", {"Colors","Casting"})  
  		GUI("color", "Channeling Ability", {"Colors","Channeling"})  		
  		GUI("color", "Ability Font", {"Colors","Ability"}) 
		GUI("color", "Item Font", {"Colors","Item"}) 
  		GUI("color", "Interaction", {"Colors","Interact"})		
   end
  
  -- fills in all the values and shows the config GUI
  GUI:Show()
end

-- will get called after the apply-button has been hit and the settings have been changed
function PartyCast_config.SettingsChanged()
PartyCast.CreateFrame()
end